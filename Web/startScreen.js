/*
 * JS for startScreen generated by Appery.io
 */

Appery.getProjectGUID = function() {
    return '9d3d247c-c468-4ce6-aa16-f51d7ebe9a5f';
};

function navigateTo(outcome, useAjax) {
    Appery.navigateTo(outcome, useAjax);
}

// Deprecated


function adjustContentHeight() {
    Appery.adjustContentHeightWithPadding();
}

function adjustContentHeightWithPadding(_page) {
    Appery.adjustContentHeightWithPadding(_page);
}

function setDetailContent(pageUrl) {
    Appery.setDetailContent(pageUrl);
}

// Appery push registration service
var _pushRegistrationApperyService = new Appery.RestService({
    'url': 'https://api.appery.io/rest/push/reg',
    'dataType': 'json',
    'type': 'post',
    'contentType': 'application/json',
});

Appery.AppPages = [{
    "name": "authenticate",
    "location": "authenticate.html"
}, {
    "name": "startScreen",
    "location": "startScreen.html"
}, {
    "name": "strategy_luggage",
    "location": "strategy_luggage.html"
}, {
    "name": "signinsignup",
    "location": "signinsignup.html"
}, {
    "name": "strategies",
    "location": "strategies.html"
}, {
    "name": "mainScreen",
    "location": "mainScreen.html"
}];

startScreen_js = function(runBeforeShow) {

    /* Object & array with components "name-to-id" mapping */
    var n2id_buf = {
        'logo': 'startScreen_logo'
    };

    if ("n2id" in window && window.n2id !== undefined) {
        $.extend(n2id, n2id_buf);
    } else {
        window.n2id = n2id_buf;
    }

    if (navigator.userAgent.indexOf("IEMobile") != -1) {
        // Fixing issue https://github.com/jquery/jquery-mobile/issues/5424 on Windows Phone
        $("div[data-role=footer]").css("bottom", "-36px");
    }

    Appery.CurrentScreen = 'startScreen';

    /*
     * Nonvisual components
     */
    var datasources = [];

    /*
     * Appery Push-notification registration service
     */
    var _registerPushApperyDS = new Appery.DataSource(_pushRegistrationApperyService, {
        'onComplete': function(jqXHR, textStatus) {
            Appery.refreshScreenFormElements("startScreen");
        },
        'onSuccess': function(data) {
            console.info('App successfully registered with Appery.io Push service');
            $(document).trigger('pushregistrationsuccess');
        },
        'onError': function(jqXHR, textStatus, errorThrown) {
            $(document).trigger('pushregistrationfail');
        },
        'responseMapping': [],
        'requestMapping': [{
            'PATH': ['type'],
            'TRANSFORMATION': function(value) {
                return Appery.getTargetPlatform();
            }
        }, {
            'PATH': ['token'],
            'ID': '___local_storage___',
            'ATTR': 'pushNotificationToken'
        }, {
            'PATH': ['deviceID'],
            'ID': '___local_storage___',
            'ATTR': 'pushNotificationDeviceID'
        }, {
            'PATH': ['X-Appery-App-Id'],
            'HEADER': true,
            'TRANSFORMATION': function(value) {
                return Appery.getProjectGUID();
            }
        }, {
            'PATH': ['timeZone'],
            'ID': '___local_storage___',
            'ATTR': 'deviceTimeZone'
        }]
    });

    /*
     * Events and handlers
     */

    // Before Show
    var startScreen_beforeshow = function() {
            Appery.CurrentScreen = "startScreen";
            for (var idx = 0; idx < datasources.length; idx++) {
                datasources[idx].__setupDisplay();
            }
        };

    // On Load
    var startScreen_onLoad = function() {
            startScreen_elementsExtraJS();

            Appery("mobilecontainer").css('background-image', 'url("files/views/assets/image/bcg_new-01.png")');

            // TODO fire device events only if necessary (with JS logic)
            startScreen_deviceEvents();
            startScreen_windowEvents();
            startScreen_elementsEvents();
        };

    // screen window events
    var startScreen_windowEvents = function() {

            $('#startScreen').bind('pageshow orientationchange', function() {
                var _page = this;
                adjustContentHeightWithPadding(_page);
            });

        };

    // device events
    var startScreen_deviceEvents = function() {
            document.addEventListener("deviceready", function() {

                $(document).bind("resume", function() {
                    PushNotification.getPendingNotifications(function(status) {
                        var notifications = status.notifications;
                        for (var i = 0; i < notifications.length; ++i) {
                            PushNotification.notificationCallback(notifications[i]);
                        }
                    });
                });
                $(document).bind("pushinit", function() {
                    localStorage.setItem('pushNotificationToken', arguments[1].deviceToken);
                    _registerPushApperyDS.execute({});
                });

                var pushNotification = PushNotification;
                pushNotification.getDeviceUniqueIdentifier(function(status) {
                    localStorage.setItem('pushNotificationDeviceID', status);
                });

                var offset = new Date().getTimezoneOffset();
                var hr = parseInt(offset / (-60));
                var min = -offset - hr * 60;
                var tmin = '' + min;
                var timezone = 'UTC' + (hr > 0 ? '+' + hr : hr) + ':' + (tmin.length > 1 ? tmin : '0' + tmin);
                localStorage.setItem('deviceTimeZone', timezone);

                pushNotification.registerDevice({
                    alert: false,
                    badge: false,
                    sound: false,
                    senderid: ''
                }, function(status) {
                    $(document).trigger('pushinit', status);
                });
            });
        };

    // screen elements extra js
    var startScreen_elementsExtraJS = function() {
            // screen (startScreen) extra code

        };

    // screen elements handler
    var startScreen_elementsEvents = function() {
            $(document).on("click", "a :input,a a,a fieldset label", function(event) {
                event.stopPropagation();
            });

            $(document).off("click", '#startScreen_mobilecontainer [name="logo"]').on({
                click: function() {
                    if (!$(this).attr('disabled')) {
                        Appery.navigateTo('authenticate', {
                            reverse: false
                        });

                    }
                },
            }, '#startScreen_mobilecontainer [name="logo"]');

        };

    $(document).off("pagebeforeshow", "#startScreen").on("pagebeforeshow", "#startScreen", function(event, ui) {
        startScreen_beforeshow();
    });

    if (runBeforeShow) {
        startScreen_beforeshow();
    } else {
        startScreen_onLoad();
    }
};

$(document).off("pagecreate", "#startScreen").on("pagecreate", "#startScreen", function(event, ui) {
    Appery.processSelectMenu($(this));
    startScreen_js();
});