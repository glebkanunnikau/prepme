/*
 * Service settings
 */
var prepme_settings = {
    "database_url": "https://api.appery.io/rest/1/db",
    "database_id": "5324617ae4b08e804971d69a"
}

/*
 * Services
 */

var prepme_preps_create_service = new Appery.RestService({
    'url': '{database_url}/collections/preps',
    'dataType': 'json',
    'type': 'post',
    'contentType': 'application/json',

    'serviceSettings': prepme_settings
});

var prepme_preps_delete_service = new Appery.RestService({
    'url': '{database_url}/collections/preps/{object_id}',
    'dataType': 'json',
    'type': 'delete',

    'serviceSettings': prepme_settings
});

var prepme_preps_list_service = new Appery.RestService({
    'url': '{database_url}/collections/preps',
    'dataType': 'json',
    'type': 'get',

    'serviceSettings': prepme_settings
});

var prepme_preps_read_service = new Appery.RestService({
    'url': '{database_url}/collections/preps/{object_id}',
    'dataType': 'json',
    'type': 'get',

    'serviceSettings': prepme_settings
});

var prepme_preps_update_service = new Appery.RestService({
    'url': '{database_url}/collections/preps/{object_id}',
    'dataType': 'json',
    'type': 'put',
    'contentType': 'application/json',

    'serviceSettings': prepme_settings
});

var prepme_preps_query_service = new Appery.RestService({
    'url': '{database_url}/collections/preps',
    'dataType': 'json',
    'type': 'get',

    'serviceSettings': prepme_settings
});
var GeolocationService = new Appery.GeolocationService({});

var prepme_tips_read_service = new Appery.RestService({
    'url': '{database_url}/collections/tips/{object_id}',
    'dataType': 'json',
    'type': 'get',

    'serviceSettings': prepme_settings
});

var prepme_tips_create_service = new Appery.RestService({
    'url': '{database_url}/collections/tips',
    'dataType': 'json',
    'type': 'post',
    'contentType': 'application/json',

    'serviceSettings': prepme_settings
});

var Geocoding_service = new Appery.RestService({
    'url': 'https://api.appery.io/rest/1/code/2127bc05-7a7e-4a4a-b462-bcbd45ae1ac6/exec',
    'dataType': 'json',
    'type': 'post',
    'contentType': false,
});

var prepme_tips_query_service = new Appery.RestService({
    'url': '{database_url}/collections/tips',
    'dataType': 'json',
    'type': 'get',

    'serviceSettings': prepme_settings
});

var prepme_tips_list_service = new Appery.RestService({
    'url': '{database_url}/collections/tips',
    'dataType': 'json',
    'type': 'get',

    'serviceSettings': prepme_settings
});

var prepme_tips_delete_service = new Appery.RestService({
    'url': '{database_url}/collections/tips/{object_id}',
    'dataType': 'json',
    'type': 'delete',

    'serviceSettings': prepme_settings
});

var prepme_tips_update_service = new Appery.RestService({
    'url': '{database_url}/collections/tips/{object_id}',
    'dataType': 'json',
    'type': 'put',
    'contentType': 'application/json',

    'serviceSettings': prepme_settings
});