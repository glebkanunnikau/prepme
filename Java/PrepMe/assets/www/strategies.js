/*
 * JS for strategies generated by Appery.io
 */

Appery.getProjectGUID = function() {
    return '9d3d247c-c468-4ce6-aa16-f51d7ebe9a5f';
};

function navigateTo(outcome, useAjax) {
    Appery.navigateTo(outcome, useAjax);
}

// Deprecated


function adjustContentHeight() {
    Appery.adjustContentHeightWithPadding();
}

function adjustContentHeightWithPadding(_page) {
    Appery.adjustContentHeightWithPadding(_page);
}

function setDetailContent(pageUrl) {
    Appery.setDetailContent(pageUrl);
}

Appery.AppPages = [{
    "name": "authenticate",
    "location": "authenticate.html"
}, {
    "name": "startScreen",
    "location": "startScreen.html"
}, {
    "name": "strategy_luggage",
    "location": "strategy_luggage.html"
}, {
    "name": "signinsignup",
    "location": "signinsignup.html"
}, {
    "name": "strategies",
    "location": "strategies.html"
}, {
    "name": "mainScreen",
    "location": "mainScreen.html"
}];

strategies_js = function(runBeforeShow) {

    /* Object & array with components "name-to-id" mapping */
    var n2id_buf = {
        'mobilelabel_1': 'strategies_mobilelabel_1',
        'label': 'strategies_label',
        'luggage': 'strategies_luggage',
        'seventytwo': 'strategies_seventytwo',
        'everyday': 'strategies_everyday',
        'kit': 'strategies_kit',
        'week': 'strategies_week'
    };

    if ("n2id" in window && window.n2id !== undefined) {
        $.extend(n2id, n2id_buf);
    } else {
        window.n2id = n2id_buf;
    }

    if (navigator.userAgent.indexOf("IEMobile") != -1) {
        // Fixing issue https://github.com/jquery/jquery-mobile/issues/5424 on Windows Phone
        $("div[data-role=footer]").css("bottom", "-36px");
    }

    Appery.CurrentScreen = 'strategies';

    /*
     * Nonvisual components
     */
    var datasources = [];

    /*
     * Events and handlers
     */

    // Before Show
    var strategies_beforeshow = function() {
            Appery.CurrentScreen = "strategies";
            for (var idx = 0; idx < datasources.length; idx++) {
                datasources[idx].__setupDisplay();
            }
        };

    // On Load
    var strategies_onLoad = function() {
            strategies_elementsExtraJS();

            Appery("mobilecontainer").css('background-image', 'url("files/views/assets/image/PrepMeCamo3.png")');

            // TODO fire device events only if necessary (with JS logic)
            strategies_deviceEvents();
            strategies_windowEvents();
            strategies_elementsEvents();
        };

    // screen window events
    var strategies_windowEvents = function() {

            $('#strategies').bind('pageshow orientationchange', function() {
                var _page = this;
                adjustContentHeightWithPadding(_page);
            });

        };

    // device events
    var strategies_deviceEvents = function() {
            document.addEventListener("deviceready", function() {

                $(document).bind("resume", function() {
                    PushNotification.getPendingNotifications(function(status) {
                        var notifications = status.notifications;
                        for (var i = 0; i < notifications.length; ++i) {
                            PushNotification.notificationCallback(notifications[i]);
                        }
                    });
                });
            });
        };

    // screen elements extra js
    var strategies_elementsExtraJS = function() {
            // screen (strategies) extra code

        };

    // screen elements handler
    var strategies_elementsEvents = function() {
            $(document).on("click", "a :input,a a,a fieldset label", function(event) {
                event.stopPropagation();
            });

            $(document).off("click", '#strategies_mobilecontainer [name="luggage"]').on({
                click: function() {
                    if (!$(this).attr('disabled')) {
                        Appery.navigateTo('strategy_luggage', {
                            reverse: false
                        });

                    }
                },
            }, '#strategies_mobilecontainer [name="luggage"]');

        };

    $(document).off("pagebeforeshow", "#strategies").on("pagebeforeshow", "#strategies", function(event, ui) {
        strategies_beforeshow();
    });

    if (runBeforeShow) {
        strategies_beforeshow();
    } else {
        strategies_onLoad();
    }
};

$(document).off("pagecreate", "#strategies").on("pagecreate", "#strategies", function(event, ui) {
    Appery.processSelectMenu($(this));
    strategies_js();
});